import pygame, sys
import random
from pygame.sprite import Sprite


def load_character(img_id):
    img = pygame.transform.scale(pygame.image.load('assets/images/' + img_id + '.png'), (250, 250))
    return img


def load_zombie(img_id):
    img = pygame.transform.scale(pygame.image.load('assets/images/' + img_id + '.png'), (100, 100))
    flipped = pygame.transform.flip(img, True, False)
    return flipped


class Character(Sprite):

    def __init__(self, pos_x, pos_y):
        super().__init__()
        self.sprites = []
        for i in range(9):
            self.sprites.append(load_character("character/idle/tile" + str(i)))
        self.current_sprite = 0
        self.image = self.sprites[self.current_sprite]
        self.rect = self.image.get_rect()
        self.rect.topleft = [pos_x, pos_y]
        self.lives = 3
        self.dead = False

    def current_sprite_number(self):
        return self.current_sprite

    def attack(self):
        self.sprites = []
        for i in range(12):
            self.sprites.append(load_character("character/attack/tile" + str(i)))

    def death(self):
        self.dead = True
        self.sprites = []
        for i in range(23):
            self.sprites.append(load_character("character/death/tile" + str(i)))

    def run(self):
        if not self.dead:
            self.sprites = []
            for i in range(6):
                self.sprites.append(load_character("character/run/tile" + str(i)))

    def reset(self):
        self.sprites = []
        for i in range(9):
            self.sprites.append(load_character("character/idle/tile" + str(i)))
        self.current_sprite = 0
        self.image = self.sprites[int(self.current_sprite)]

    def hurt(self):
        if(self.lives <= 0):
            self.death()
        else:
            self.lives = self.lives - 1
            self.sprites = []
            for i in range(4):
                self.sprites.append(load_character("character/hurt/tile" + str(i)))
        

    def update(self, speed):
        if self.current_sprite == 22:
            self.current_sprite = 22
        else:
            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                # self.sprites = []
                # for i in range(9):
                #     self.sprites.append(load_img("character/idle/tile" + str(i)))
                self.current_sprite = 0
            self.image = self.sprites[int(self.current_sprite)]


class Zombie(Sprite):

    def __init__(self, pos_x, pos_y):
        super().__init__()
        self.sprites = []
        for i in range(1, 7):
            self.sprites.append(load_zombie("zombie/idle/Idle" + str(i)))
        self.current_sprite = 0
        self.image = self.sprites[self.current_sprite]
        self.rect = self.image.get_rect()
        self.rect.topleft = [pos_x, pos_y]
        self.alive = True
        self.cooldown = 0
    
    def add_cooldown(self):
        self.cooldown = 60000

    def lower_cooldown(self):
        if(self.cooldown > 0):
            self.coolown = self.cooldown - 1

    def is_cooldown(self):
        if self.cooldown == 0:
            return False
        return True

    def attack(self):
        self.sprites = []
        for i in range(1, 7):
            self.sprites.append(load_zombie("zombie/attack/Attack" + str(i)))

    def death(self):
        self.alive = False
        self.sprites = []
        for i in range(1, 9):
            self.sprites.append(load_zombie("zombie/death/Dead" + str(i)))

    def run(self):
        self.sprites = []
        for i in range(6):
            self.sprites.append(load_character("character/run/tile" + str(i)))

    def reset(self):
        self.sprites = []
        for i in range(9):
            self.sprites.append(load_character("character/idle/tile" + str(i)))
        self.current_sprite = 0
        self.image = self.sprites[int(self.current_sprite)]

    def update(self, speed):
        if self.current_sprite == 7:
            self.current_sprite = 7
        else:
            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                # self.sprites = []
                # for i in range(9):
                #     self.sprites.append(load_img("character/idle/tile" + str(i)))
                self.current_sprite = 0

            self.image = self.sprites[int(self.current_sprite)]

    def movement(self, movement):
        self.rect.topleft = [self.rect.x - movement, self.rect.y]


def main(best_score = 0):
    # General setup
    movement = 0
    current_score = 0
    pygame.init()
    clock = pygame.time.Clock()

    # Game screen
    screen = pygame.display.set_mode((1080, 640))
    pygame.display.set_caption("Hack and Slash")
    bg_grass = pygame.transform.scale(pygame.image.load('assets/images/background/ground.png'), (1440, 253))
    bg_sky = pygame.transform.scale(pygame.image.load('assets/images/background/bg2_2.png'), (1440, 824))
    bg_x = 0
    bg_y = -390

    #scoreBoard
    pygame.font.init()
    my_font = pygame.font.SysFont('monospace', 30)

    # character sprite
    character_sprite_group = pygame.sprite.Group()
    character = Character(-30, 330)
    character_sprite_group.add(character)

    # zombie sprites
    zombie_sprite_group = pygame.sprite.Group()
    for x in range(5):
        zombie_sprite_group.add(Zombie(random.randint(200, 1440), 430))

    while True:
        for zombie in zombie_sprite_group:
            zombie.lower_cooldown()
            zombie.movement(movement)

            if abs(character.rect.x - zombie.rect.x) < 150 and not zombie.is_cooldown() and zombie.alive:
                zombie.attack()
                character.hurt()
                movement = 0
                zombie.add_cooldown()

            if character.rect.x - 100 > zombie.rect.x:
                rand = random.randint(1440, 1600)
                zombie.rect.x = character.rect.x + rand
                zombie.__init__(zombie.rect.x, 430)
            
            if abs(character.rect.x - zombie.rect.x) < 200 and character.current_sprite_number() == 11:  
                if zombie.alive:
                    current_score += 1
                zombie.death()

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN and event.key == pygame.K_f:
                if current_score > best_score:
                    main(current_score)
                else:
                    main(best_score)

            if character.dead is not True and event.type == pygame.KEYDOWN and event.key == pygame.K_d:
                movement = 10
                character.run()

            if character.dead is not True and event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                character.attack()
                movement = 0

            if character.dead is not True and event.type == pygame.KEYUP and event.key == pygame.K_d:
                movement = 0
                character.reset()


        bg_x += movement

        # HUD
        score_text = my_font.render('score:' + str(current_score), False, (255, 255, 255))
        lives_text = my_font.render('lives: ' + str(character.lives), False, (255, 255, 255))
        best_score_text = my_font.render('best score: ' + str(best_score), False, (255, 255, 255))

        # Drawing
        screen.blit(bg_sky, (0, -420))
        screen.blit(bg_grass, (-bg_x, -bg_y))
        screen.blit(bg_grass, (-bg_x + 1440, -bg_y))
        screen.blit(score_text, (0,0))
        screen.blit(best_score_text, (0,25))
        screen.blit(lives_text, (0,50))

        if bg_x >= 1440:
            bg_x = 0

        zombie_sprite_group.draw(screen)
        zombie_sprite_group.update(0.25)

        character_sprite_group.draw(screen)
        character_sprite_group.update(1)

        pygame.display.flip()
        pygame.display.update()
        clock.tick(45)

bestScore = 0
main(bestScore)
